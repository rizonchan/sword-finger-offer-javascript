/**
 描述
在一个字符串(0<=字符串长度<=10000，全部由字母组成)中找到第一个只出现一次的字符,并返回它的位置,
 如果没有则返回 -1（需要区分大小写）.（从0开始计数）
 */

 //方法1：使用indexof
const str="google"
function FirstNotRepeatingChar1(str){
    for(let i=0;i<str.length;i++){
        if(str.indexOf(str[i]) == str.lastIndexOf(str[i])){
            return i;
        }
    }
    return -1;
}

//方法2：使用map()
function FirstNotRepeatingChar2(str){
    let map=new Map();
    for(let i of str){
         map[i]=(map[i]||0)+1;//是否有map[i]没有就为0；有就为当前值；
    }
    for(let i in str){
        if(map[str[i]]==1) return i;
    }
    return -1;
}

FirstNotRepeatingChar2(str)