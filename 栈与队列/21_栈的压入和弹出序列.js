/**
 * 输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否可能为该栈的弹出顺序。
 * 假设压入栈的所有数字均不相等。
 * 例如序列1,2,3,4,5是某栈的压入顺序，序列4,5,3,2,1是该压栈序列对应的一个弹出序列，但4,3,5,1,2就不可能是该压栈序列的弹出序列。
 * （注意：这两个序列的长度是相等的）
 */

function IsPopOrder(pushV, popV)
{
    // write code here
    //添加一个辅助栈
    let tmp=[];
    let index=0;//辅助栈的Index
    for(let i=0;i<pushV.length;i++){
        tmp.push(pushV[i]);//辅助栈入站
        while(tmp.length&&tmp[tmp.length-1]==popV[index]){//如果辅助栈的栈顶元素等于popV的元素，辅助栈出栈该元素，然后继续下一个元素。
            tmp.pop();
            index++;
        }
    }
    return tmp.length!==0;
}
