// 定义栈的数据结构，请在该类型中实现一个能够得到栈中所含最小元素的min函数，并且调用 min函数、push函数 及 pop函数 的时间复杂度都是 O(1)
// push(value):将value压入栈中
// pop():弹出栈顶元素
// top():获取栈顶元素
// min():获取栈中最小元素

const stackArr = [];
function push(node)
{
    // write code here
    return stackArr.push(node);
}
function pop()
{
    // write code here
    return stackArr.pop();
}
function top()
{
    // write code here
    return stackArr[stackArr.length - 1];
}
function min()
{
    // write code here
    return Math.min(...stackArr);
}