function ReverseList(pHead){
    let pre=null;//充当尾部的null;
    //循环
    while(pHead){
        let cur=pHead;//使cur指向头结点
        pHead=pHead.next;//将Phead设置为下一次循环的头节点；
        cur.next=pre;//头结点的下一个节点指向null，之后指向的是上一次的未接点；
        pre=cur;//pre指向头结点
    }
    return pre;
}