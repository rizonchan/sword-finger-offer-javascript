/**
 * 
描述
输入一颗二叉树的根节点和一个整数，按字典序打印出二叉树中结点值的和为输入整数的所有路径。
路径定义为从树的根结点开始往下一直到叶结点所经过的结点形成一条路径
 */

function FindPath(root, expectNumber){
    let res=[];
    function dfs(root,arr,sum){
        if(!root) return
        arr.push(root.val);//当前节点的值存入数组
        if(!root.left&&!root,right){
            if(sum==root.val){
                res.push(arr);//如果递归到最后sum和root的值相等，就将这个加入数组
            }
        }
        dfs(root.left,[...arr],sum-root.val)
        dfs(root.right,[...arr],sum-root.val)
    }
    dfs(root,[],expectNumber);
    return res;
}
