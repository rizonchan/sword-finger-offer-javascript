/**
 * 
 描述
给定一个二叉树其中的一个结点，请找出中序遍历顺序的下一个结点并且返回。
注意，树中的结点不仅包含左右子结点，同时包含指向父结点的next指针。
下图为一棵有9个节点的二叉树。树中从父节点指向子节点的指针用实线表示，从子节点指向父节点的用虚线表示
 */
/*function TreeLinkNode(x){
    this.val = x;
    this.left = null;
    this.right = null;
    this.next = null;
}*/
//先去进行分析，如果该节点有右节点，则去判断右节点有左子树，有左子树就是要找的，没有左子树，那么该几点的右节点就是
//如果没有右节点，判断父节点，如果父节点的左子树正好是该节点，那么父节点就是要找的，不是再去招父节点的父节点
function GetNext(pNode){
    if(pNode==null) return null;
    if(pNode.right!=null){//右节点存在
        let p=pNode.right;
        while(p.left!=null){//左子树存在，一直去找左子树
            p=p.left;//这个就是要找的
        }
        return p;//左子树直接不存在就是
    }
    while(pNode.next!=null){
        if(pNode.next.left==pNode) return p.next;
        pNode=pNode.next;
    }
}