/**
 描述
给定一个二叉树，返回该二叉树的之字形层序遍历，（第一层从左向右，下一层从右向左，一直这样交替）
 */
/* function TreeNode(x) {
    this.val = x;
    this.left = null;
    this.right = null;
} */

function Print(pRoot)
{
    // write code here
    let floor=1;
    let queue=[];
    let res=[];
    if(!pRoot) return res;
    queue.push(pRoot)
    while(queue.length!=0){
        let tmp=[];
        let len=queue.length;
        while(len){
            let node=queue.shift();
            tmp.push(node.val);
            if(node.left) queue.push(node.left);
            if(node.right) queue.push(node.right);
            len--;
        }
        if(floor%2===1){
            res.push(tmp);
        }else{
            res.push(tmp.reverse())
        }
        floor++;
    }
    return res;
}