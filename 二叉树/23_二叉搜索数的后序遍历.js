/**
 * 
 * 描述
输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历的结果。
如果是则返回true,否则返回false。
假设输入的数组的任意两个数字都互不相同。（ps：我们约定空树不是二叉搜索树）
 */
function VerifySquenceOfBST(sequence)
{
    // write code here
    if (sequence.length === 0) {
        return false;
    }
    var len = sequence.length - 1;
    while(len) {
        var count = 0;
        while(sequence[count] < sequence[len]) {
            ++count;
        }
        while(sequence[count] > sequence[len]) {
            ++count;
        }
        if (count < len) {
            return false;
        }
        --len;
    }
    return true;
     
}
var test=[4,9,10,12,16,14,11];
var res=VerifySquenceOfBST(test)
console.log(res);