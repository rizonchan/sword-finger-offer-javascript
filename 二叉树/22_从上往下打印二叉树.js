/* function TreeNode(x) {
    this.val = x;
    this.left = null;
    this.right = null;
} */
function PrintFromTopToBottom(root)
{
    // write code here
    if(!root) return false;
    const res=[];
    const queue=[root];
    while(queue.length>0){
        const  shiftnode=queue.shift();//使用队列存储结构
        res.push(shiftnode.val);
        if(shiftnode.left){
            queue.push(shiftnode.left);
        }
        if(shiftnode.right){
            queue.push(shiftnode.right)
        }
    }
    return res
}