/**
 *
 描述
输入一棵二叉树，判断该二叉树是否是平衡二叉树。
在这里，我们只需要考虑其平衡性，不需要考虑其是不是排序二叉树
平衡二叉树（Balanced Binary Tree），
具有以下性质：它是一棵空树或它的左右两个子树的高度差的绝对值不超过1，并且左右两个子树都是一棵平衡二叉树。
注：我们约定空树是平衡二叉树。
 */
//思路：首先要判断子树的深度，他们的决定值要不超过1，之后在判断是否是平衡二叉树，所以有两层递归
function IsBalanced_Solution(pRoot){
    if(!pRoot) return true;//空树是平衡二叉树
    if(Math.abs(depth(pRoot.left)-depth(pRoot.right))>1) return false;
    return IsBalanced_Solution(pRoot.left)&&IsBalanced_Solution(pRoot.right)
}
function depth(node){
    if(!node) return 0;
    let left=depth(node.left);
    let right=depth(node.right);
    return Math.max(left,right)+1
}