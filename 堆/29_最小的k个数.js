/**
 *
 描述
给定一个数组，找出其中最小的K个数。例如数组元素是4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4。
0 <= k <= input.length <= 10000
0 <= input[i] <= 10000
 * @param {*} input 
 * @param {*} k 
 * @returns 
 */
function GetLeastNumbers_Solution(input, k)
{
    // write code here
    var length=input.length;
    var newarr=input.sort((a,b)=>{
        return a-b;
    })
    return newarr.slice(0,k)
}