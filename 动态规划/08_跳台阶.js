function jumpFloor(number){
    const dp=[];
    dp[0]=1;
    dp[1]=1;
    for(let i=2;i<=number;i++){
        dp[i]=dp[i-1]+dp[i-2];
    }
    return dp[number];
}
var  res=jumpFloor(7);
console.log('result',res);