/**
 * 
 描述
我们可以用2*1的小矩形横着或者竖着去覆盖更大的矩形。
请问用n个2*1的小矩形无重叠地覆盖一个2*n的大矩形，从同一个方向看总共有多少种不同的方法？
比如n=3时，2*3的矩形块有3种不同的覆盖方法(从同一个方向看)：
 */
function rectCover(number)
{
    // write code here
    const dp=[];
    dp[0]=0
    dp[1]=1;
    dp[2]=2;
    for(let i=3;i<=number;i++){
        dp[i]=dp[i-1]+dp[i-2];
    }
    return dp[number];
}